# Lateral Challenge

OS Requirements:
* python3 (https://www.python.org/downloads/)
* pip (https://pip.pypa.io/en/stable/installing/)
* pipenv (https://docs.pipenv.org/en/latest/install/)

## Installation

Clone the repository
```bash
git clone git@gitlab.com:julio.lacerda/lateral-challenge.git
```

Open the directory
```bash
cd lateral-challenge
```

Install all the application requirements
```bash
pipenv install
```

Activate pipenv environment
```bash
pipenv shell
```

Initialize the service
```bash
python -m lateral.server
```

Configuration is available at ./etc/lateral/conf.toml

It defaults to the below:
```bash
[development]

[development.server]
port = 8888

[development.lateral]
[development.lateral.api]
key = "f53dd4aea5bfc8ecd850fcbe1b08921e"
url = "https://news-api.lateral.io/"
content_type = "application/json"
```

Tests are available at

Initialize the service
```bash
python -m pytest
```

Exit the pipenv environment
```bash
exit
```
