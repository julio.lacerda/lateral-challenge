import tornado.ioloop
import tornado.web

from lateral.entry import app
from lateral.settings import conf

if __name__ == "__main__":
    app.listen(conf.SERVER.PORT)
    tornado.ioloop.IOLoop.current().start()
