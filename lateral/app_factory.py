import tornado.ioloop
import tornado.web

from lateral.api.routes import routes


def make_app():
    return tornado.web.Application(routes)
