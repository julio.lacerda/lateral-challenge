from lateral.api.common import routes as common_routes
from lateral.api.recommendation import routes as recommendation_routes

__all__ = ["routes"]


routes = [*common_routes.routes, *recommendation_routes.routes]
