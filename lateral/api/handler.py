from tornado.web import RequestHandler


class Handler(RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header(
            "Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS"
        )
        self.set_header(
            "Access-Control-Allow-Headers",
            "access-control-allow-origin,authorization,content-type,withcredentials",
        )
        self.set_header("Content-Type", "application/json")

    def options(self):
        self.set_status(204)
        self.finish()
