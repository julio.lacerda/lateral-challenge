import unittest

from lateral.api.recommendation.api import LateralAPI
from lateral.api.recommendation.schemas.make import MakeRecommendationSchema


class MakeRecomendationTest(unittest.TestCase):
    def test_200_ok(self):
        text = "Boris Johnson has suggested the UK could withhold part of its divorce bill from Brussels in a no-deal Brexit, insisting the sum would not 'strictly speaking' be owed in such circumstances."
        response = LateralAPI.make_recommendation({"text": text})
        assert response.status_code == 200

    def test_400_wrong_payload(self):
        text = "Boris Johnson has suggested the UK could withhold part of its divorce bill from Brussels in a no-deal Brexit, insisting the sum would not 'strictly speaking' be owed in such circumstances."
        schema = MakeRecommendationSchema()
        is_valid, data = schema.is_valid({"text1": text})
        assert not is_valid

    def test_400_no_payload(self):
        schema = MakeRecommendationSchema()
        is_valid, data = schema.is_valid()
        assert not is_valid
