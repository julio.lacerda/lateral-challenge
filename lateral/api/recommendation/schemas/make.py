from marshmallow import fields

from lateral.api.schema import Schema


class MakeRecommendationSchema(Schema):
    text = fields.Str()
