from lateral.api.recommendation.handlers import make

__all__ = ["routes"]


routes = [(r"/recommendations", make.MakeRecommendationHandler)]
