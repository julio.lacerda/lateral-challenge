import json

import requests

from lateral.settings import conf


class LateralAPI:
    @staticmethod
    def make_recommendation(data):
        url = conf.LATERAL.API.URL + "documents/similar-to-text"
        payload = json.dumps({"text": data["text"]})
        headers = {
            "Content-Type": conf.LATERAL.API.CONTENT_TYPE,
            "Subscription-Key": conf.LATERAL.API.KEY,
        }
        return requests.post(url, data=payload, headers=headers)
