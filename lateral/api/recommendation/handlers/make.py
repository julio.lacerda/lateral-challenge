import json

from lateral.api.handler import Handler
from lateral.api.recommendation.api import LateralAPI
from lateral.api.recommendation.schemas.make import MakeRecommendationSchema


class MakeRecommendationHandler(Handler):
    class Meta:
        schema = MakeRecommendationSchema

    def post(self):
        body = json.loads(self.request.body)
        schema = self.Meta.schema()
        is_valid, data = schema.is_valid(body)

        if is_valid:
            response = LateralAPI.make_recommendation(data)
            self.write(response.content)
        else:
            self.set_status(400)
            self.write(json.dumps(dict(data.messages)))
