import pytest

from lateral.entry import entry


@pytest.fixture
def app():
    return entry


@pytest.mark.gen_test
def test_200_ok(http_client, base_url):
    response = yield http_client.fetch(base_url)
    assert response.code == 200
