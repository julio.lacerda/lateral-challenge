from datetime import datetime

from lateral.api.handler import Handler


class CurrentTimestampHandler(Handler):
    def get(self):
        self.write({"message": str(int(datetime.now().timestamp()))})
