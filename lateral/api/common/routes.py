from lateral.api.common.handlers import timestamp

__all__ = ["routes"]


routes = [(r"/", timestamp.CurrentTimestampHandler)]
